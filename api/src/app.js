const Testing = require("./Testing")

const cors = require('cors')
var express = require('express')
var api = express()
api.use(cors())

const port = process.env.PORT || 8080

const testing = new Testing()
const sendSalute = testing.salute()

api.get('/', function (req, res) {
  res.send(sendSalute)
})

api.listen(8081, function () {
  console.log('Example api listening on port 8081!')
})

api.listen(port, () => console.log(`Almuerzo en el Berlanga!!  ${port}!`))
